#ifndef MOBILEESCAPEROOM_HPP
#define MOBILEESCAPEROOM_HPP

#include <pigpiopp.h>
#include <vector>
#include <stdexcept>
#include <mutex>
#include <condition_variable>
#include <string>
#include <SFML/Audio.hpp>
#include <cstdint>
#include <iostream>

class MobileEscapeRoom
{
    std::mutex mutex;
    bool is_ready;
    std::condition_variable cv_ready;
    std::vector<unsigned> input_pins;
    unsigned reset_pin;
    unsigned exit_pin;
    std::vector<unsigned> output_pins;
    std::vector<sf::SoundBuffer> sound_buffers;
    std::vector<sf::Sound> sounds;
    std::vector<bool> riddles_solved;
    pigpiopp::client pigpio;

public:
    MobileEscapeRoom(std::vector<unsigned> input_pins,
                     unsigned reset_pin,
                     unsigned exit_pin,
                     std::vector<unsigned> output_pins,
                     std::vector<std::string> sounds)
        : mutex(),
          is_ready(false),
          cv_ready(),
          input_pins(std::move(input_pins)),
          reset_pin(reset_pin),
          exit_pin(exit_pin),
          output_pins(std::move(output_pins)),
          sound_buffers(this->input_pins.size()),
          sounds(this->input_pins.size()),
          riddles_solved(this->input_pins.size()),
          pigpio()
    {
        if (this->input_pins.size() != this->output_pins.size()) {
            throw std::runtime_error("Number of input and output pins does not match!");
        }
        if (this->input_pins.size() != sounds.size()) {
            throw std::runtime_error("Number of pins and sounds does not match!");
        }
        for (std::size_t i = 0; i < sounds.size(); i++) {
            bool success = sound_buffers[i].loadFromFile(sounds[i]);
            if (!success) {
                throw std::runtime_error("Cannot load sound " + sounds[i]);
            }
            this->sounds[i].setBuffer(sound_buffers[i]);
        }
    }

    void run();

private:
    bool all_riddles_solved();
};

#endif // MOBILEESCAPEROOM_HPP
